# haskell-intero atom package

This package provides IDE-like functionality for Haskell development in Atom using [Intero](https://github.com/commercialhaskell/intero).

## Requirements
- [Stack](https://haskellstack.org/) must be installed
- [language-haskell](https://atom.io/packages/language-haskell) atom package for syntax highlighting

## TODO
☑ Launch Intero from the Stack project root path of the currently open Haskell source file

☑ Display status bar icon, with spinner

☑ run `:reload` on file save in the running Intero repl and display compilation errors

☑ highlighting compilation errors inline in Haskell source files

☑ Create a pane with the active Intero repl. Hidden by default, but can be toggled with a command. Visible by default in atom dev mode. Allows for interaction with the repl.

☐ Print information about the current environment, such as path to Stack executable, Stack project etc. in the Intero repl pane on/before Intero startup.

☐ Limit the size of the Intero repl, to avoid a huge memory consumption for long running sessions

☐ Install Intero for the current Stack snapshot (`stack build intero`), after prompting the user for confirmation

☐ In case the correct GHC version is not installed and Intero fails to install, run `stack setup` after prompting the user for confirmation

☐ only run `:reload` on file save if the file 1) is a Haskell source file and 2) it belongs to the loaded project

☐ create hyperlinks from the compilation errors to file locations (using [hyperclick](https://atom.io/packages/hyperclick))
