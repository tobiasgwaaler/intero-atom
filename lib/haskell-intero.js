'use babel';

import { CompositeDisposable } from 'atom';

import Intero from './intero';
import Linter from './linter';
import StatusBar from './status-bar';
import Repl from './repl';
import {extractVariable, extractVariableFromFile} from './haskell-src';

export default {

  activate(state) {
    atom.commands.add('atom-text-editor', {
      'haskell-intero:goToDefinition': this.goToDefinition.bind(this)
    });

    this.subscriptions = new CompositeDisposable();

    this.repl = new Repl();
    this.repl.onInput(input => this.intero._execute(input));

    this.statusBar = new StatusBar();
    this.statusBar.onClick(() => this.repl.toggle());

    const currentFileDirectory = atom.workspace.getActiveTextEditor().getBuffer().file.getParent();
    this.intero = new Intero(currentFileDirectory.getPath());
    this.intero.on('data', (data) => this.repl.write(data));
    this.intero.on('busy', (cmd) => {
      this.statusBar.animate()
      this.repl.write(cmd);
    });
    this.intero.on('ready', () => this.statusBar.stop());
    this.intero.start()
      .then(messages => this.displayCompileErrors(messages));

    atom.workspace.observeTextEditors((editor) => {
      editor.onDidSave((event) => {
        this.reload();
      })
    });
  },

  goToDefinition(event) {
    const textEditor = atom.workspace.getActiveTextEditor();
    const cursorBufferPos = textEditor.getCursorBufferPosition();
    const line = textEditor.lineTextForBufferRow(cursorBufferPos.row);
    const {varName, varStart, varEnd} = extractVariable(line, cursorBufferPos.column);
    const locationReq = {
      filePath: textEditor.getPath(),
      fromRow: cursorBufferPos.row + 1,
      fromColumn: varStart + 1,
      toRow: cursorBufferPos.row + 1,
      toColumn: varEnd + 1,
      variableName: varName
    };
    this.intero.locAt(locationReq)
      .then( ({filePath, startLine, startColumn}) => {
        atom.workspace.open(filePath, {initialLine: startLine, initialColumn: startColumn});
      });
  },

  consumeLinter(indieRegistry) {
    const interoLinter = indieRegistry.register({name: 'Intero'})
    this.subscriptions.add(interoLinter)
    this.linter = new Linter(interoLinter);
  },

  consumeStatusBar(statusBar) {
    this.statusBarTile = statusBar.addRightTile({item: this.statusBar.getDOM(), priority: 100});
  },

  deactivate() {
    this.subscriptions.dispose();
    this.statusBarTile.destroy();
    this.intero.destroy();
    this.repl.destroy();
  },

  reload() {
    this.linter.clear();
    this.intero.reload()
      .then( messages => this.displayCompileErrors(messages));
  },

  displayCompileErrors(messages) {
    messages.errors.forEach( msg => {
      const linterMsg = {
        type: msg.severity,
        text: msg.message,
        filePath: msg.file,
        range: msg.position
      }
      this.linter.add(linterMsg);
    });
  }

};
