'use babel'

export default class InteroStdoutParser {
  constructor({onSymbolLocation}) {
    this.onSymbolLocation = onSymbolLocation;
  }

  consume(line) {
    // matches '/some/path/src.hs:(1,2)-(1,10)'
    const symbolLocationRegex = /(.*\.hs):\((\d+),(\d+)\)-\((\d+),(\d+)\)/;
    const symbolLocationMatch = line.match(symbolLocationRegex);
    if(symbolLocationMatch){
      const [all, filePath, rowStart, colStart, rowEnd, colEnd] = symbolLocationMatch;
      this.onSymbolLocation({
        filePath,
        row: parseInt(rowStart, 10),
        column: parseInt(colStart, 10)
      })
    }
  }

}
