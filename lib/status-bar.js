'use babel';

export default class StatusBar {

  constructor() {
    this.interoStatusBar = document.createElement('intero-status');
    this.interoStatusBar.classList.add('inline-block');

    this.interoSpinnerIcon = document.createElement('span');
    this.interoSpinnerIcon.classList.add('intero-status-spinner-icon');
    this.interoStatusBar.appendChild(this.interoSpinnerIcon);

    const interoSpinnerText = document.createElement('span');
    interoSpinnerText.textContent = 'Intero';
    interoSpinnerText.classList.add('intero-status-spinner-text');
    this.interoStatusBar.appendChild(interoSpinnerText);
  }

  onClick(callback) {
    this.interoStatusBar.addEventListener('click', callback);
  }

  getDOM() {
    return this.interoStatusBar;
  }

  animate(){
    this.interoSpinnerIcon.classList.add('intero-status-spinner-icon-animate');
  }

  stop(){
    this.interoSpinnerIcon.classList.remove('intero-status-spinner-icon-animate');
  }
}
