'use babel';

const IS_WAITING = 0;
const IS_PARSING_MESSAGE = 1;

export default class InteroStderrParser {

  constructor({onCompilerMessage}) {
    this.onCompilerMessage = onCompilerMessage;
    this.reset();
  }

  reset(){
    this.parserState = IS_WAITING;
    this.currentMessage = {};
  }

  consume(line) {
    if(this.parserState === IS_WAITING){
      const message = this.parseMessageStart(line);
      if(message){
        this.currentMessage = message;
        this.parserState = IS_PARSING_MESSAGE;
      }

    } else if(this.parserState === IS_PARSING_MESSAGE){
      if(line === ''){
        if(this.currentMessage.text === ''){
          // sometimes a stray empty line can occur, skip it
        } else {
          this.onCompilerMessage(this.currentMessage);
          this.reset();
        }
      } else {
        this.currentMessage.text += line;
      }
    }
  }

  parseMessageStart(line){
    const messageRegex = new RegExp(
      "\\s*" +             // ignore all leading whitespace
      "(.*\\.hs)" +        // file path
      ":([0-9]+)" +        // line number
      ":([0-9]+): " +      // column number
      "([a-z]*):?" +       // optional severity: error | warn
      ".*"                 // compiler flags, on the form [-W<flag>]
    );

    const parsedMessage = line.match(messageRegex);
    if(parsedMessage) {
      const [all, filePath, lineNum, colNum, type] = parsedMessage;
      return {
        type: type ? type : 'error',
        text: '',
        range: [[parseInt(lineNum)-1, parseInt(colNum)-1], [parseInt(lineNum)-1, parseInt(colNum)]],
        filePath,
        severity: type
      };
    }

    return false;
  }

}
