'use babel'

export default class Repl {
  constructor() {
    this._onInput = () => console.log('onInput is not defined for the repl');
    this.render();
  }

  onInput(callback) {
    this._onInput = callback;
  }

  render() {
    this.repl = document.createElement('div');
    this.repl.classList.add('intero-repl-panel');
    this.repl.classList.add('native-key-bindings');
    this.repl.setAttribute('tabindex', -1);

    this.replOutput = document.createElement('pre');
    this.replOutput.classList.add('intero-repl-panel--output');
    this.repl.appendChild(this.replOutput);

    this.replInput = document.createElement('div');
    this.replInput.classList.add('intero-repl-panel--input');
    this.replInput.setAttribute('contenteditable', 'true');
    this.replInput.addEventListener('keydown', (event) => {
      if(event.keyCode === 13 && !event.shiftKey){
        event.preventDefault();
        this._onInput(this.replInput.innerText);
        this.replInput.innerText = '';
      }
    });
    this.repl.appendChild(this.replInput);

    const visible = atom.devMode;
    this.panel = atom.workspace.addBottomPanel({item: this.repl, visible});
  }

  write(text) {
    // scroll to the bottom after appending the text only if the
    // repl was already scrolled to the bottom
    let scrollToBottom = this.replOutput.scrollHeight - this.replOutput.scrollTop === this.replOutput.clientHeight;
    this.replOutput.innerText += text;
    if(scrollToBottom){
      this.replOutput.scrollTop = this.replOutput.scrollHeight;
    }
  }

  toggle() {
    this.panel.isVisible() ? this.panel.hide() : this.panel.show();
  }

  destroy() {
    this.panel.destroy();
  }
}
