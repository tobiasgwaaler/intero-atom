'use babel'

const IDENTIFIER_SEPARATORS = [].concat(
  [' ', '\n', '\t', '(', ')', ',', ';', '[', ']', '`', '{', '}', '!', '#', '$', '%'],
  ['&', '*', '+', '.', '/', '<', '=', '>', '?', '@',	'\\', '^', '|', '-', '~']
);

export function extractVariable(line, index) {
  const leftSlice = line.slice(0, index);
  const locationsLeft =
    IDENTIFIER_SEPARATORS
      .map((separator) => leftSlice.lastIndexOf(separator))
      .concat([-1]);
  const identifierStart = Math.max(...locationsLeft) + 1;

  const rightSlice = line.slice(index);
  const locationsRight =
    IDENTIFIER_SEPARATORS
      .map((separator) => rightSlice.indexOf(separator))
      .filter((n) => n > -1)
      .concat([rightSlice.length]);
  const identifierStop = Math.min(...locationsRight) + index;

  return {
    varName: line.slice(identifierStart, identifierStop),
    varStart: identifierStart,
    varEnd: identifierStop
  };
}

export function extractVariableFromFile(filePath, row, column) {
  return atom.workspace
            .open(filePath, {activatePane: false, activateItem: false})
            .then( textEditor => extractVariable(textEditor.lineTextForBufferRow(row), column));
}
