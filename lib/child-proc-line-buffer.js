'use babel'

export default class ChildProcLineBuffer {
  constructor(onLineRead, endOfLine){
    this.endOfLine = ['\n'];
    if(endOfLine){
      this.endOfLine = this.endOfLine.concat(endOfLine);
    }
    this.onLineRead = onLineRead;
    this.buffer = '';
  }

  consume(chunk){
    this.buffer += chunk.utf8Slice();

    let indexAfterEol;
    do {
      indexAfterEol = this.indexAfterFirstEOL();
      if(indexAfterEol > -1) {
        this.onLineRead(this.buffer.slice(0, indexAfterEol));
        this.buffer = this.buffer.slice(indexAfterEol);
      }
    } while (indexAfterEol > -1);
  }

  indexAfterFirstEOL() {
    return endIndexOfFirst(this.buffer, this.endOfLine);
  }
}

const endIndexOfFirst = (str, searchValues) => {
  const ixs =
    searchValues
      .map(searchValue => indexAfter(str, searchValue))
      .filter(nonNegative);
  return ixs.length > 0 ? Math.min.apply(Math, ixs) : -1;
}

const nonNegative = n => n > -1;

const indexAfter = (str, searchValue) => {
  let i = str.indexOf(searchValue)
  if(i > -1){
    return i + searchValue.length;
  }
  return -1;
}
