'use babel';

import {BufferedProcess} from 'atom';
import EventEmitter from 'events';
import os from 'os';
import path from 'path';
import net from 'net';
import {spawn} from 'child_process';

import InteroOutBuffer from './intero-out-buffer';
import {parseCompileErrorsAndWarnings} from './parsers/reload-parser';
import {parseLocAt} from './parsers/locat-parser';

export default class Intero extends EventEmitter {

  constructor(currentFilePath) {
    super();
    this.currentFilePath = currentFilePath;
  }

  start() {
    this.emit('busy', `Determining Stack project root directory for ${this.currentFilePath}\n`);
    return stackProjectRootDir(this.currentFilePath)
      .then( stackProjectRootDir => {
        this.emit('busy', `Stack project root directory found: ${stackProjectRootDir}\n`);
        this.socketFilePath = mkSocketFilePath();
        this.interoOut = new InteroOutBuffer();
        this.server = net.createServer((outStream) => {
          outStream.on('data', (data) => {
            this.interoOut.write(data);
            this.emit('data', data);
          });
        });
        this.emit('busy', `Listening to socket for Intero stdout/stderr: ${this.socketFilePath}\n`);
        this.server.listen(this.socketFilePath);

        const compileErrors = this.interoOutputPromise().then(parseCompileErrorsAndWarnings);
        const socket = net.connect(this.socketFilePath, () => {
          this.emit('busy', 'Connected to socket\n');
          this.interoProcess = spawn(
            'stack', ['ghci', '--with-ghc', 'intero',
                      `--ghci-options="-ghci-script=${dotGhciPath()}"`,
                      '--ghci-options="-ferror-spans"',
                      '--ghci-options="-Wall"'
                    ],
            { cwd:   stackProjectRootDir,
              stdio: ['pipe', socket, socket] }
          );
          this.emit('busy', `Starting Intero in ${stackProjectRootDir}\n`);
        });
        return compileErrors;
      });
  }

  reload() {
    return this._execute(":reload").then(parseCompileErrorsAndWarnings);
  }

  locAt({filePath, fromRow, fromColumn, toRow, toColumn, variableName}){
    const cmd = `:loc-at ${filePath} ${fromRow} ${fromColumn} ${toRow} ${toColumn} ${variableName}`;
    return this._execute(cmd).then(parseLocAt);
  }

  /* Execute cmd in Intero and return the response as a Promise  */
  _execute(cmd) {
    if(!cmd.endsWith('\n')){
      cmd = cmd + '\n';
    }
    this.emit('busy', cmd);
    const resultPromise = this.interoOutputPromise();
    this.interoProcess.stdin.write(cmd, "utf-8", null);
    return resultPromise;
  }

  interoOutputPromise(){
    return new Promise((resolve, reject) => {
      this.interoOut.once('data', (data) => {
        this.emit('ready');
        resolve(data);
      });
    });
  }

  destroy() {
    this.interoProcess.kill()
    this.server.close();
  }

}

const mkSocketFilePath = () => path.join(os.tmpdir(), 'intero-atom-buffer-' + process.pid + '.sock');

const dotGhciPath = () => atom.packages.resolvePackagePath('haskell-intero') + '/atom-intero-ghci-script'

const stackProjectRootDir = (currentFilePath) =>
  new Promise((resolve, reject) => {
    new BufferedProcess({
      command: 'stack',
      args:    ['path', '--project-root'],
      stdout:  (line) => resolve(line.replace('\n', '')),
      options: {cwd: currentFilePath}
    });
  });
