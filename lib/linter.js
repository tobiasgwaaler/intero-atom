'use babel'

import {isEqual, find} from 'lodash';

export default class Linter {
  constructor(indieLinter){
    this.indieLinter = indieLinter;
    this.compilerMessages = [];
  }

  clear(){
    this.indieLinter.deleteMessages();
    this.compilerMessages = [];
  }

  add(compilerMessage){
    if(!find(compilerMessages, (msg) => isEqual(msg, compilerMessage)){
      this.compilerMessages.push(compilerMessage);
      this.indieLinter.setMessages(this.compilerMessages);
    }
  }
}
