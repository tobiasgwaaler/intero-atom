import {Writable} from 'stream';

export const interoOutputStream = new Writable({
  write(chunk, encoding, callback) {
    console.log(chunk);
    callback();
  }
});
