'use babel'

export function parseLocAt(str) {
  const messageRegex = new RegExp(
    "(.*\\.hs):"      // file path
+   "\\("
+      "([0-9]*)"     // start line
+      ","
+      "([0-9]*)"     // start column
+   "\\)"
+   "-"
+   "\\("
+      "([0-9]*)"    // end line
+      ","
+      "([0-9]*)"    // end column
+   "\\)"
  );
  const [all, filePath, startLine, startColumn, endLine, endColumn] = messageRegex.exec(str);
  return {
    filePath,
    startLine: startLine - 1,      // to zero based
    startColumn: startColumn - 1,  // to zero based
    endLine: endLine - 1,          // to zero based
    endColumn: endColumn - 1       // to zero based
  };
}
