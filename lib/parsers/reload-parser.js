'use babel'

export const parseCompileErrorsAndWarnings = (str) => {
  const messageRegex = new RegExp(
    "^(.*\\.hs):"                                              // 1 file name
+   "(?:"
+         "([0-9]+):([0-9]+):"                                // 2 3 line:col
+      "|"
+         "(?:([0-9]+):([0-9]+)-([0-9]+):)"                   // 4 5 6 line:col-col
+      "|"
+         "(?:\\(([0-9]+),([0-9]+)\\)-\\(([0-9]+),([0-9]+)\\):)"  // 7 8 9 10 (line,col)-(line,col)
+    ")"
+    "( error:| warning:)?"
+    "( \\[.*\\])?"
+    "\\n?"
+    "((?:[ \\t].*\\n)*)"
    , 'mgi'                // Flags: multiline, global and ignore case
  );

  const result = {
    errors: []
  }

  let parsedMessage;
  do {
    parsedMessage = messageRegex.exec(str);
    if(parsedMessage) {
      const file = parsedMessage[1];
      let position;
      if(parsedMessage[2] && parsedMessage[3]){
        position = [
                    [ parseInt(parsedMessage[2]) - 1, parseInt(parsedMessage[3]) - 1 ],
                    [ parseInt(parsedMessage[2]) - 1, parseInt(parsedMessage[3])     ]
                   ];
      } else if(parsedMessage[4] && parsedMessage[5] && parsedMessage[6]) {
        position = [
                    [ parseInt(parsedMessage[4]) - 1, parseInt(parsedMessage[5]) - 1 ],
                    [ parseInt(parsedMessage[4]) - 1, parseInt(parsedMessage[6])     ]
                   ];
      } else {
        position = [
                    [ parseInt(parsedMessage[7]) - 1, parseInt(parsedMessage[8])  - 1 ],
                    [ parseInt(parsedMessage[9]) - 1, parseInt(parsedMessage[10])     ]
                   ];
      }
      const severity = parsedMessage[11] ? parsedMessage[11] : 'error';
      const message = parsedMessage[13];
      result.errors.push({
          file,
          severity: severity.toLowerCase().includes('warning') ? 'warning' : 'error',
          position,
          message
        });
      };
  } while(parsedMessage)

  return result;
}
