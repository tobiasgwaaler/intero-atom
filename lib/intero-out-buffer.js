'use babel'

import EventEmitter from 'events';

const GHCI_PROMPT = 'atom-intero-prompt>';

export default class InteroOutBuffer extends EventEmitter {
  constructor(){
    super();
    this.buffer = '';
  }

  write(data) {
    this.buffer += data.utf8Slice();
    if(this.buffer.endsWith(GHCI_PROMPT)){
      console.log(this.buffer);
      this.emit('data', this.buffer.slice());
      this.buffer = '';
    }
  }

}
