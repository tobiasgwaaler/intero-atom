'use babel'

import {parseCompileErrorsAndWarnings} from '../lib/parsers/reload-parser';

const compilerOutput = "\
[1 of 2] Compiling Lib              ( /Users/tobiasgw/src/intero-atom-package/haskell-test-project/haskell-intero-atom-testproject/src/Lib.hs, /Users/tobiasgw/src/intero-atom-package/haskell-test-project/haskell-intero-atom-testproject/.stack-work/odir/Lib.o ) [flags changed]\n\
[2 of 2] Compiling Main             ( /Users/tobiasgw/src/intero-atom-package/haskell-test-project/haskell-intero-atom-testproject/app/Main.hs, /Users/tobiasgw/src/intero-atom-package/haskell-test-project/haskell-intero-atom-testproject/.stack-work/odir/Main.o )\n\
\n\
/Users/tobiasgw/src/intero-atom-package/haskell-test-project/haskell-intero-atom-testproject/app/Main.hs:12:3: error:\n\
    • Couldn't match expected type ‘IO ()’\n\
                  with actual type ‘Int -> IO ()’\n\
    • Probable cause: ‘someFunc’ is applied to too few arguments\n\
      In a stmt of a 'do' block: someFunc\n\
      In the expression:\n\
        do { let x = aFunc_\n\
                 y = R x\n\
                 ....;\n\
             someFunc }\n\
      In an equation for ‘main’:\n\
          main\n\
            = do { let x = ...\n\
                       ....;\n\
                   someFunc }\n\
\n\
/X.hs:10:30: error: [-Wunused-local-binds]\n\
   Some error\n\
   over multiple lines\n\
\n\
/Home.hs:17:62:\n\
   Not in scope: ‘yy’\n\
\n\
Foundation.hs:169:21: Warning: Defined but not used: ‘email’\n\
Failed, modules loaded: Lib.\n\
atom-intero-prompt>"

const expectedParseResult = {
  errors: [{
    file:    '/Users/tobiasgw/src/intero-atom-package/haskell-test-project/haskell-intero-atom-testproject/app/Main.hs',
    position:    [[12,3],[12,3]],
    severity: 'error',
    message: "\
    • Couldn't match expected type ‘IO ()’\n\
                  with actual type ‘Int -> IO ()’\n\
    • Probable cause: ‘someFunc’ is applied to too few arguments\n\
      In a stmt of a 'do' block: someFunc\n\
      In the expression:\n\
        do { let x = aFunc_\n\
                 y = R x\n\
                 ....;\n\
             someFunc }\n\
      In an equation for ‘main’:\n\
          main\n\
            = do { let x = ...\n\
                       ....;\n\
                   someFunc }\n"
  },{
    file: '/X.hs',
    position: [[10,30],[10,30]],
    severity: 'error',
    message: "\
   Some error\n\
   over multiple lines\n"
  },{
    file: '/Home.hs',
    position: [[17,60],[17,60]],
    severity: 'error',
    message: "\
   Not in scope: ‘yy’\n"
  }],
}

describe('reload-parser', () => {
  it('parses single error message', () => {
    const actualParsedResult = parseCompileErrorsAndWarnings(compilerOutput)
    expectedParseResult.errors.forEach((expectedError, i) => {
      expect(actualParsedResult.errors[i].file).toEqual(expectedError.file);
      expect(actualParsedResult.errors[i].line).toEqual(expectedError.line);
      expect(actualParsedResult.errors[i].column).toEqual(expectedError.column);
      expect(actualParsedResult.errors[i].severity).toEqual(expectedError.severity);
      expect(actualParsedResult.errors[i].message).toEqual(expectedError.message);
    })

  })
})
