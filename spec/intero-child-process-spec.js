'use babel'

import {spawn} from 'child_process';

// Useful for debugging, but not a spec per se
xdescribe('spawn Intero child process', () => {
  it('starts with ghci-flags', () => {
    const dotGhciPath = () => atom.packages.resolvePackagePath('haskell-intero') + '/atom-intero-ghci-script'
    const interoProcess = spawn(
      'stack', ['ghci', '--with-ghc', 'intero',
                `--ghci-options="-ghci-script=${dotGhciPath()}"`,
                '--ghci-options="-ferror-spans"',
                '--ghci-options="-Wall"'
              ],
      { cwd:   '/Users/tobiasgw/src/intero-atom-package/haskell-test-project/haskell-intero-atom-testproject',
        stdio: ['pipe', 'pipe', 'pipe'] }
    );

    interoProcess.stdout.on('data', (data) => { console.log(data.toString()) });
    interoProcess.stderr.on('data', (data) => { console.log(data.toString()) });
    interoProcess.on('close', (code) => { console.log(`child process exited with code ${code}`) });
    debugger;
    interoProcess.kill();
  })
})
