'use babel'

import {extractVariable} from '../lib/haskell-src';

describe('haskell-src', () => {
  it('extracts alphanumeric variable name', () => {
    const res = extractVariable('   let simpleVariableName1 = 1', 10);
    expect(res.varName).toBe('simpleVariableName1');
    expect(res.varStart).toBe(7);
    expect(res.varEnd).toBe(26);
  })

  it('extracts variable name from call site', () => {
    const res = extractVariable('main = someFunc', 12);
    expect(res.varName).toBe('someFunc');
    expect(res.varStart).toBe(7);
    expect(res.varEnd).toBe(15);
  })

  it('extracts variable name from first row', () => {
    const res = extractVariable('main = someFunc', 2);
    expect(res.varName).toBe('main');
    expect(res.varStart).toBe(0);
    expect(res.varEnd).toBe(4);
  })
})
