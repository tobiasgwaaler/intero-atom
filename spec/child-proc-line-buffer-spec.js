'use babel'

import ChildProcLineBuffer from '../lib/child-proc-line-buffer';

describe('ChildProcLineBuffer', () => {
  it('emits lines separated by \\n', () => {
    const lines = []
    const lineBuffer = new ChildProcLineBuffer((line) => lines.push(line))

    lineBuffer.consume(Buffer.from('some dat', 'utf-8'));
    lineBuffer.consume(Buffer.from('a\nAnd s', 'utf-8'));
    lineBuffer.consume(Buffer.from('ome more\ndata for you\n', 'utf-8'));

    expect(lines).toEqual(['some data\n', 'And some more\n', 'data for you\n']);
  })

  it('emits lines separated by additional custom EOL', () => {
    const lines = []
    const onLineRead = (line) => lines.push(line);
    const lineBuffer = new ChildProcLineBuffer(onLineRead, '<custom eol>')

    lineBuffer.consume(Buffer.from('some data\nand some ', 'utf-8'));
    lineBuffer.consume(Buffer.from('more<custom eol>without this', 'utf-8'));

    expect(lines).toEqual(['some data\n', 'and some more<custom eol>']);
  })
})
